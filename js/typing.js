// wizytowka section offer
var wrapper = document.querySelector(".wrapper");
var text = document.querySelector(".text");

var textCont = text.textContent;
text.style.display = "none";

for (var i = 0; i < textCont.length; i++) {
  (function(i) {
    setTimeout(function() {
      // Created textNode to append
      var texts = document.createTextNode(textCont[i]);
      var span = document.createElement('span');
      span.appendChild(texts);

      span.classList.add("wave");
      wrapper.appendChild(span);

    }, 75 * i);
  }(i));
}

// blog section offer
var wrapper2 = document.querySelector(".wr2");
var text2 = document.querySelector(".txt2");

var textCont2 = text2.textContent;
text2.style.display = "none";

for (var i2 = 0; i2 < textCont2.length; i2++) {
  (function(i2) {
    setTimeout(function() {
      // Created textNode to append
      var texts2 = document.createTextNode(textCont2[i2]);
      var span2 = document.createElement('span');
      span2.appendChild(texts2);

      span2.classList.add("wave");
      wrapper2.appendChild(span2);

    }, 75 * i2);
  }(i2));
}

//Lending Page section offer
var wrapper3 = document.querySelector(".wr3");
var text3 = document.querySelector(".txt3");

var textCont3 = text3.textContent;
text3.style.display = "none";

for (var i3 = 0; i3 < textCont3.length; i3++) {
  (function(i3) {
    setTimeout(function() {
      // Created textNode to append
      var texts3 = document.createTextNode(textCont3[i3]);
      var span3 = document.createElement('span');
      span3.appendChild(texts3);

      span3.classList.add("wave");
      wrapper3.appendChild(span3);

    }, 75 * i3);
  }(i3));
}

// Strona www oparta na CMS section offer
var wrapper4 = document.querySelector(".wr4");
var text4 = document.querySelector(".txt4");

var textCont4 = text4.textContent;
text4.style.display = "none";

for (var i4 = 0; i4 < textCont4.length; i4++) {
  (function(i4) {
    setTimeout(function() {
      // Created textNode to append
      var texts4 = document.createTextNode(textCont4[i4]);
      var span4 = document.createElement('span');
      span4.appendChild(texts4);

      span4.classList.add("wave");
      wrapper4.appendChild(span4);
    }, 75 * i4);
  }(i4));
}

// Sklep internetowy section offer
var wrapper5 = document.querySelector(".wr5");
var text5 = document.querySelector(".txt5");

var textCont5 = text5.textContent;
text5.style.display = "none";

for (var i5 = 0; i5 < textCont5.length; i5++) {
  (function(i5) {
    setTimeout(function() {
      // Created textNode to append
      var texts5 = document.createTextNode(textCont5[i5]);
      var span5 = document.createElement('span');
      span5.appendChild(texts5);

      span5.classList.add("wave");
      wrapper5.appendChild(span5);

    }, 75 * i5);
  }(i5));
}